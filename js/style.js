//1(Опишіть своїми словами, що таке метод об'єкту) Метод об'єкту - це функція, яка є властивістю об'єкта. Вона може використовуватися для виконання певних дій на об'єкті або з об'єктом.

//2(Який тип даних може мати значення властивості об'єкта?) Рядки, числа, масив рядків

//3(Об'єкт це посилальний тип даних.) Означає, що змінній, яка містить об'єкт, присвоюється не сам об'єкт, а посилання на нього(вказує на місцезнаходження об'єкта в пам'яті комп'ютера)




let firstName = prompt("Ваше ім'я");
let lastName = prompt("Ваше прізвище");

function createNewUser(name, last) {
  let user = {
    name: name,
    last: last,
    getLogin: function() {
      return this.name.charAt(0).toLowerCase() + this.last.toLowerCase();
    }
  };
  return user;
}

let newUser = createNewUser(firstName, lastName);
console.log(newUser.name, newUser.last);
console.log(newUser.getLogin());